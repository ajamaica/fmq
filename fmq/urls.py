from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.contrib.auth.decorators import login_required
from afiliacion.views import MyPasswordChangeView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fmq.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^accounts/password/change/$', login_required(MyPasswordChangeView.as_view()), name='my_password_change'),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'}),
    #(r'^accounts/password/change/$', 'allauth.account.views.password_change' , {'next_page': '/'}),
    (r'^accounts/', include('allauth.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('sinergia.urls')),
    url(r'^dashboard/', include('afiliacion.urls')),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', { 'document_root': settings.STATIC_ROOT, }),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', { 'document_root': settings.MEDIA_ROOT, }),
)
