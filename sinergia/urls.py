from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fmq.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'sinergia.views.index',name='index'),
    url(r'^guidelines/$', 'sinergia.views.guidelines',name='guidelines'),
    url(r'^osc/(\d+)$', 'sinergia.views.osc',name='osc'),
    url(r'^evaluacion/(\d+)$', 'sinergia.views.evaluacion',name='evaluacion'),
    url(r'^oscs/$', 'sinergia.views.oscs',name='oscs'),
    
)
