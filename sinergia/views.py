#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.db.models import Q
from afiliacion.models import *
from sinergia.models import *
from sinergia.forms import *
from afiliacion.forms import *
from django.conf import settings
from django.core.mail import EmailMessage
import pusher
from django.shortcuts import get_list_or_404, get_object_or_404
import operator
from django.views.decorators.csrf import csrf_exempt
import datetime
from dateutil.relativedelta import relativedelta
# Create your views here.

def objeto_social():
    objeto_social = (
        ("1", "Asistencia social"),
        ("2", "Apoyo a la alimentación popular"),
        ("3", "Promoción de  la participación ciudadana"),
        ("4", "Asistencia jurídica"),
        ("5", "Desarrollo de los pueblos indígenas"),
        ("6", "Promoción de la equidad de género"),
        ("7", "Atención a personas con discapacidad"),
        ("8", "Desarrollo comunitario urbano o rural"),
        ("9", "Apoyo en la defensa y promoción de los derechos humanos"),
        ("10", "Promoción del deporte"),
        ("11", "Atención de la salud y cuestiones sanitarias"),
        ("12", "Protección del Medio Ambiente"),
        ("13", "Fomento educativo, cultural, artístico, científico y tecnológico"),
        ("14", "Fomento de acciones para mejorar la economía popular"),
        ("15", "Protección civil"),
        ("16", "Fortalecimiento de otras organizaciones de la sociedad civil"),
        ("17", "Defensa de los derechos de los consumidores"),
        ("18", "Fortalecimiento del tejido social y la seguridad ciudadana"),
        ("19", "Otras"),
    )

    return objeto_social

@csrf_exempt
def index(request):
    index = True
    template = "index.html"
    categorias_all = Categoria.objects.all()
    
    banner = Banner.objects.all()
    
    if 'buscar_asistida' in request.GET:
        return oscs(request)
    
    causas_sociales = objeto_social
    estados = Estado.objects.all().order_by('nombre')
    municipios = []

    for e in estados:
        municipios.append(Municipio.objects.filter(estado=e.id).order_by('nombre'))

    return render_to_response(template, context_instance=RequestContext(request,locals()))


def sinergia_avanzada(oscs, request):

    parametros = ''

    if len(oscs) == 0:
        osc = Afiliacion.objects.filter(status="A")

    if 'municipio' in request.GET:
        municipio = request.GET['municipio']
        parametros += '&estado='+municipio
        oscs = oscs.filter(municipio=municipio)
    
    if 'causa_social' in request.GET:
        causa_social = str(request.GET['causa_social'])
        parametros += '&causa_social='+causa_social
        oscs = oscs.filter(actividades=causa_social)
        
    if 'grupo_atencion' in request.GET:
        grupo_atencion = request.GET.getlist('grupo_atencion')
        
        if 'bebes' in grupo_atencion:
            parametros += '&grupo_atencion=bebes'
            oscs = oscs.filter(impacto_social__bebes__gt=0)


        if 'menores_12' in grupo_atencion:
            parametros += '&grupo_atencion=menores_12'
            oscs = oscs.filter(Q(impacto_social__ninas__gt=0) | Q(impacto_social__ninos__gt=0))

        if 'jovenes_18' in grupo_atencion:
            parametros += '&grupo_atencion=jovenes_18'
            oscs = oscs.filter(impacto_social__jovenes__gt=0)

        if 'adultos' in grupo_atencion:
            parametros += '&grupo_atencion=adultos'
            oscs = oscs.filter(Q(impacto_social__adultas__gt=0) | Q(impacto_social__adultos__gt=0))
            
        if 'adultos_mayores' in grupo_atencion:
            parametros += '&grupo_atencion=adultos_mayores'
            oscs = oscs.filter(Q(impacto_social__mayores_mujeres__gt=0) | Q(impacto_social__mayores_hombres__gt=0))
    
    oscs = oscs.order_by('nombre')
    parametros += '&buscar_asistida='

    lista = [oscs, parametros]

    return lista


def guidelines(request):
    template = "guidelines.html"
    return render_to_response(template, context_instance=RequestContext(request,locals()))


def osc(request, pk):
    directorio = True
    osc = get_object_or_404(Afiliacion, pk = pk)
    categorias_all = Categoria.objects.all()
    
    if request.method == 'POST':

        if 'enviacorreoS' in request.POST:

            empresa_object = Empresa.objects.get(id=request.POST['empresaS'])
            sinergia_object = Sinergia.objects.get(empresa__id=request.POST['empresaS'])
            
            subject = "FMQ: Tu apoyo es importante"
            p_begin = '<p align="justify">'
            osc_nombre = '<strong>' + sinergia_object.osc.nombre + '</strong>'
            osc_nombre_contacto = '<strong>' + sinergia_object.osc.responsable + '</strong>'
            osc_telefono = '<strong>' + sinergia_object.osc.telefono + '</strong>'
            osc_correo = '<strong>' + sinergia_object.osc.email + '</strong>'
            oscs_link = '<a href="'+settings.URL+'/oscs/">Ver organizaciones</a>'

            mensaje = request.POST['mensaje']
            mensaje = mensaje.replace('\n', '<br>')
            mensaje = mensaje.replace('<osc_nombre>', osc_nombre)
            mensaje = mensaje.replace('<osc_nombre_contacto>', osc_nombre_contacto)
            mensaje = mensaje.replace('<osc_telefono>', osc_telefono)
            mensaje = mensaje.replace('<osc_correo>', osc_correo)
            mensaje = mensaje.replace('<oscs_link>', oscs_link)

            p_end = "</p>"
            mensaje = p_begin + mensaje + p_end
            fr = "contacto@fundacionmercedqro.com"
            t = sinergia_object.empresa.email
            mensaje = EmailMessage(subject, mensaje, fr, [t, ])
            mensaje.content_subtype = "html"  # Main content is now text/html
            mensaje.send()

            return HttpResponseRedirect('/oscs/')

        else:            
            form = Multi_Sinergia(request.POST, request.FILES)        
            if form.is_valid():
                ambas = form.save(commit=False)
                empresa = ambas["empresa"]
                sinergia = ambas["sinergia"]
                empresa.save()
                sinergia.osc = osc
                empresa_id = empresa.id
                sinergia.empresa = empresa
                sinergia.save()
                numero = 2
                
            else:
                numero = 1
    else:
        form = Multi_Sinergia()

    materiales = osc.necesidadesmateriales_set.all()
    humanas = osc.necesidadeshumanas_set.all()
    capacitacion = osc.necesidadescapacitacion_set.all()
    sinergias = osc.sinergias_exitosas()
    

    fecha_inicio = datetime.datetime.now()
    three_yrs_ago = datetime.datetime.now() - relativedelta(years=1)
    
    valor_horas = 0
    asistenciasPrueba = Asistencia.objects.filter(afiliacion =osc, fecha__gt=three_yrs_ago)


    for horas in asistenciasPrueba:
        valor_horas += horas.horas
         
    template = "crowdfunding_detail.html"

    if 'buscar_asistida' in request.GET:
        return oscs(request)

    causas_sociales = objeto_social
    estados = Estado.objects.all().order_by('nombre')
    municipios = []

    for e in estados:
        municipios.append(Municipio.objects.filter(estado=e.id).order_by('nombre'))

    return render_to_response(template, context_instance=RequestContext(request,locals()))


def evaluacion(request, pk):

    sinergia = get_object_or_404(Sinergia, pk=pk)
    osc = sinergia.osc

    if sinergia.status == 'E'  and (not sinergia.comentario_empresa or not sinergia.empresa.url):
        valido = 1
        #Valida para calificar

        if request.method == 'POST':
            #Se envió el formulario
            i = {
                'sinergia': sinergia, 
                'empresa': sinergia.empresa
                }
            forma = Multi_EmpresaEvalua(request.POST, request.FILES,instance=i)
            
            if forma.is_valid():
                object_forma = forma.save(commit=False)
                sinergia_form = object_forma['sinergia']
                sinergia_form.save()

                empresa_form = object_forma['empresa']
                empresa_form.save()

                return HttpResponseRedirect('/oscs/')
                #exito

        else:
            #Se enviará formulario
            forma = Multi_EmpresaEvalua(instance={
                'empresa': sinergia.empresa,
                'sinergia': sinergia
                })

    else:
        valido = 0
        #No se puede calificar


    template = "evaluacion.html"
    return render_to_response(template,context_instance=RequestContext(request,locals()))


def oscs(request):
    directorio = True
    osc_all = Afiliacion.objects.filter(status="A")
    categorias_all = Categoria.objects.all()

    if 'buscar_asistida' in request.GET:
        lista = sinergia_avanzada(osc_all, request)
        osc_all = lista[0]
        parametros = lista[1]

    elif 'causa_social' in request.GET:
        causa_social = str(request.GET['causa_social'])
        parametros = '&causa_social='+causa_social
        osc_all = osc_all.filter(actividades=causa_social)

    osc_all = osc_all.order_by('nombre')
    causas_sociales = objeto_social
    estados = Estado.objects.all().order_by('nombre')
    municipios = []

    for e in estados:
        municipios.append(Municipio.objects.filter(estado=e.id).order_by('nombre'))

    paginator = Paginator(osc_all, 24)
    page = request.GET.get('page')

    try:
        osc = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        osc = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        osc = paginator.page(paginator.num_pages)

    template = "crowdfunding_listing.html"
    return render_to_response(template, context_instance=RequestContext(request,locals()))




