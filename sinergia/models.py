#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from afiliacion.models import *
from django import forms
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize

class HorasAux:
    total_horas = 0
    osc = ""

    def __init__(self, total_horas,osc):
        self.total_horas = total_horas
        self.osc = osc 

class Banner(models.Model):
    banner = models.ImageField(default="media/default.png", upload_to = 'banner',help_text="Imagen gigante de inicio",verbose_name="Banner")
    banner_resized = ImageSpecField(
        source='banner',processors=[ResizeToFill(1500,617)], format='PNG',
        options={'quality': 60})
        
# Create your models here.
class Notificacion(models.Model):
    descripcion = models.TextField(help_text="Logo de tu Empresa",verbose_name="Descripción")
    url = models.CharField(max_length = 255,blank=True,null=True)
    user = models.ForeignKey(User, unique=True)
    def __unicode__(self):
        return "%s : %s " % (self.user,self.descripcion)

class Empresa(models.Model):
    logo = models.ImageField(default="media/default.png", upload_to = 'media/logo',null=True,blank=True,help_text="Logo de tu Empresa",verbose_name="Logo")
    email = models.EmailField(max_length = 254,help_text="Email de la Empresa",verbose_name="Email")
    logo_resized = ImageSpecField(
        source='logo',processors=[ResizeToFill(150,150)], format='PNG',
        options={'quality': 60})
    
    logo_smart = ImageSpecField(
        source='logo',processors=[SmartResize(150,150)],format='PNG')
    nombre = models.CharField(max_length = 255 ,help_text="Nombre de tu Empresa",verbose_name="Nombre")
    descripcion = models.CharField(max_length = 255,help_text="¿Qué hace tu Empresa?",verbose_name="Cómo quiero apoyar")
    url = models.CharField(max_length = 255,null=True,help_text="Sitio Web",verbose_name="Sitio Web")
    def __unicode__(self):
        return self.nombre

class Sinergia(models.Model):
    FIGURAS = (('P','Persona'), ('E', 'Empresa'))
    date_created = models.DateTimeField(auto_now_add = True)
    figura = models.CharField(max_length=1, choices=FIGURAS, default="P")
    comentario = models.CharField(max_length = 255,help_text="Comentario de la Sinergia",verbose_name="Comentario")
    STATUSS = (('N','Nuevo'), ('E', 'Exitoso'),('F', 'Fracaso'))
    status = models.CharField(max_length=1,
                                choices=STATUSS,
                                default="N")

    comentario_empresa = models.CharField(max_length = 255,help_text="Comentario de la Empresa",verbose_name="Platícanos tu experiencia")
    empresa = models.ForeignKey(Empresa)
    osc = models.ForeignKey(Afiliacion)

    def __unicode__(self):
        return self.status

    class Meta:
      ordering = ['-date_created']
