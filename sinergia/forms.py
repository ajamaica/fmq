#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms
from afiliacion.models import *
from django.forms.models import inlineformset_factory,formset_factory,modelformset_factory
from betterforms.multiform import MultiModelForm
from sinergia.models import *

class Sinergia_Form(forms.ModelForm):
	descripcion = forms.CharField(label="Cómo quiero apoyar",widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = Empresa
		fields = ('nombre','email', 'descripcion')
		
class Sinergia_Form2(forms.ModelForm):
	class Meta:
		model = Sinergia
		fields = ('figura',)

class SinergiaCancelar(forms.ModelForm):
	comentario = forms.CharField(widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = Sinergia
		fields = ('comentario',)

class Multi_Sinergia(MultiModelForm):
	form_classes = {
		'sinergia' : Sinergia_Form2,
		'empresa' : Sinergia_Form,
	}

######################################## BEGIN Marco

class EmpresaEvalua_Form(forms.ModelForm):
	comentario_empresa = forms.CharField(label="Platícanos tu experiencia",widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = Sinergia
		fields = ('comentario_empresa',)

class EmpresaEvalua2_Form(forms.ModelForm):
	class Meta:
		model = Empresa
		fields = ('url', 'logo',)

class Multi_EmpresaEvalua(MultiModelForm):
	form_classes = {
		'sinergia' : EmpresaEvalua_Form,
		'empresa' : EmpresaEvalua2_Form
	}

######################################## END Marco

#class AceptoSinergia_Form(forms.ModelForm):
	#nombre = forms.CharField(required=False, widget=forms.HiddenInput())
	#descripcion = forms.CharField(required=False, widget=forms.HiddenInput())
	#empresa = forms.CharField(required=False, widget=forms.HiddenInput())

	#class Meta:
		#model = Empresa
		#fields = ('url',)


class AceptoSinergia_Form2(forms.ModelForm):
	comentario = forms.CharField(widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = Sinergia
		fields = ('comentario',)



#class Multi_AceptoSinergia(MultiModelForm):
	#form_classes = {
		#'sinergia' : AceptoSinergia_Form2,
		#'empresa' : AceptoSinergia_Form
	#}