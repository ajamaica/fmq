from django.test import TestCase
from sinergia.models import *
from afiliacion.models import *
from datetime import datetime, date
from django.db import models
import pytz
from django.contrib.auth.models import User, check_password
from sinergia.forms import *

# Create your tests here.

class SinergiaViewsTestCase(TestCase):
    
    def test_sinergia_website(self):
        resp = self.client.get('/oscs/')
        self.assertEqual(resp.status_code, 200)
    
    def test_sinergia_website_single(self):
        resp = self.client.get('/osc/1')
        self.assertEqual(resp.status_code, 200)
        
class SinergiaCase(TestCase):
    fixtures = ['data_prueba']
    def setUp(self):
        super(SinergiaCase, self).setUp()
        self.empresa_object = Empresa(nombre = "empresa1", descripcion="la mejor empresa",  email = "mail@dsf.com")
        self.empresa_object.save()
        self.empresa_object = Empresa.objects.get(id=1)
        self.osc = Afiliacion.objects.get(id=1)

    def test_sinergia_crear(self):
        s = Sinergia(empresa = self.empresa_object, osc = self.osc, figura = "E", comentario = "Super bien", status = "N")
        s.save()

    def test_sinergia_empresa_exito(self):
        s = Sinergia(empresa = self.empresa_object, osc = self.osc, figura = "E", comentario = "Super bien", status = "N")
        s.save()
        si = Sinergia.objects.get(id = 1)
        si.status = "E"
        si.comentario_empresa = "Comentario"
        si.save()
        
    
    def test_sinergia_empresa_no_exito(self):
           s = Sinergia(empresa = self.empresa_object, osc = self.osc, figura = "E", comentario = "Super bien", status = "N")
           s.save()
           si = Sinergia.objects.get(id = 1)
           si.status = "N"
           si.comentario_empresa = "Comentario"
           si.save()
        