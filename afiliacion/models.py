#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from localflavor.mx.models import *
from simple_history.models import HistoricalRecords
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize
from datetime import datetime
from allauth.account.signals import password_changed
from django.dispatch import receiver
from django.contrib import messages

@receiver(password_changed)
def password_change_callback(sender, request, user, **kwargs):
    messages.success(request, 'Ha cambiado exitosamente su contraseña!', extra_tags='contrasena')

class Estado(models.Model):
    nombre = models.CharField(max_length = 255,help_text="Ejemplo: Querétaro",verbose_name="Nombre")

    def __unicode__(self):
        return self.nombre


class Municipio(models.Model):
    nombre = models.CharField(max_length = 255,help_text="Ejemplo: Corregidora",verbose_name="Nombre")
    estado = models.ForeignKey("Estado")

    def __unicode__(self):
        return self.nombre

class Afiliacion(models.Model):
    user = models.ForeignKey(User,unique = True,help_text="Escriba un nombre de usuario",verbose_name="Usuario")
    estado = models.ForeignKey(Estado,blank=True,help_text="Escriba su Estado",verbose_name="Estado", null=True)
    municipio = models.ForeignKey(Municipio,blank=True,help_text="Escriba su municipio",verbose_name="Municipio", null=True)
    history = HistoricalRecords()
    nombre = models.CharField(max_length = 255, help_text="Escribe el nombre de tu OSC", verbose_name="Nombre")
    impacto_social = models.OneToOneField("ImpactoSocial")
    date_created = models.DateTimeField(auto_now_add = True)
    date_edited = models.DateTimeField(auto_now = True)
    public = models.BooleanField(default = False)
    FIGURAS = (
        ("A", "AC"),
        ("I", "IAP"),
        ("S", "SC"),
        ("N", "No aplica"),
    )
    figura = models.CharField(max_length=1,
                                choices=FIGURAS)
    ano_constitucion = models.IntegerField(blank=True,null=True, help_text="Escribe el año en que se fundó tu OSC. Ejemplo: 1990", verbose_name="Año de Constitución")
    rfc = MXRFCField(blank=True,null=True,help_text="Ejemplo: AEOM910407BK5",verbose_name="RFC")
    cluni = models.CharField(max_length = 200,blank=True,null=True,help_text="Escribe tu CLUNI",verbose_name="CLUNI")
    donatoria_nacional = models.BooleanField(default = False,help_text="",verbose_name="¿Emiten recibos deducibles nacionales?")
    donatoria_internacional = models.BooleanField(default = False,help_text="",verbose_name="¿Emiten recibos deducibles internacionales?")
    distintivo_y_cemfi = models.BooleanField(default = False,help_text="",verbose_name="¿Cuentan el Distintivo de Institucionalidad y transparencia de CEMEFI?")
    #actividades = models.TextField(blank=True,null=True)
    domicilio_legal = models.TextField(blank=True,null=True)
    telefono  = models.CharField(max_length = 10,blank=True,null=True,help_text="Escribe tu teléfono a 10 digitos. Ejemplo: 3521004251",verbose_name="Teléfono")
    email = models.EmailField(max_length = 254,blank=True,null=True,help_text="Ejemplo: miosc@dominio.com",verbose_name="Email de tu Organización")
    website = models.CharField(max_length = 200,blank=True,null=True,help_text="Ejemplo: www.misitio.org",verbose_name="Tu Sitio Web")
    facebook = models.CharField(max_length = 200,blank=True,null=True,help_text="www.facebook.com/usuario",verbose_name="Facebook")
    twitter = models.CharField(max_length = 200,blank=True,null=True,help_text="www.twitter.com/usuario",verbose_name="Twitter")
    presidente = models.CharField(max_length = 255,blank=True,null=True,help_text="",verbose_name="Nombre del Presidente de la OSC")
    responsable = models.CharField(max_length = 255,blank=True,null=True,help_text="Escribe el nombre del Director/Coordinador",verbose_name="Nombre del Responsable de la Organización")
    cargo_responsable = models.CharField(max_length = 255,blank=True,null=True,help_text="Escriba el cargo del responsable",verbose_name="Cargo del responsable")
    email_responsable = models.EmailField(max_length = 254,blank=True,null=True,help_text="Ejemplo: juanperez@dominio.com",verbose_name="Email del Responsable")
    objeto_social = (
        ("1", "Asistencia social"),
        ("2", "Apoyo a la alimentación popular"),
        ("3", "Promoción de  la participación ciudadana"),
        ("4", "Asistencia jurídica"),
        ("5", "Desarrollo de los pueblos indígenas"),
        ("6", "Promoción de la equidad de género"),
        ("7", "Atención a personas con discapacidad"),
        ("8", "Desarrollo comunitario urbano o rural"),
        ("9", "Apoyo en la defensa y promoción de los derechos humanos"),
        ("10", "Promoción del deporte"),
        ("11", "Atención de la salud y cuestiones sanitarias"),
        ("12", "Protección del Medio Ambiente"),
        ("13", "Fomento educativo, cultural, artístico, científico y tecnológico"),
        ("14", "Fomento de acciones para mejorar la economía popular"),
        ("15", "Protección civil"),
        ("16", "Fortalecimiento de otras organizaciones de la sociedad civil"),
        ("17", "Defensa de los derechos de los consumidores"),
        ("18", "Fortalecimiento del tejido social y la seguridad ciudadana"),
        ("19", "Otras"),
    )
    actividades = models.CharField(max_length=2, choices=objeto_social, blank=True, null=True)
    tematica = models.CharField(max_length = 255,blank=True,null=True,help_text="Escriba la temática",verbose_name="Temática")
    mision = models.TextField(blank=True,null=True,help_text="Escriba la misión de la OSC",verbose_name="Misión")
    vision = models.TextField(blank=True,null=True, help_text="Escriba la visión de la OSC",verbose_name="Visión")
    pertenece_red = models.BooleanField(default = False, help_text="",verbose_name="¿Pertenece a una red?")
    nombre_red = models.CharField(default="", max_length = 255, blank=True,null=True,help_text="Escriba el nombre de la red a la que pertenece",verbose_name="Nombre de la red a la que pertenece")
    convenio = models.BooleanField(default = False, help_text="¿Tienes convenio de donativo firmado con FMQ?",verbose_name="Convenio")
    representante_autirizo = models.BooleanField(default = False, help_text="¿El Representante autorizó el uso de la información aqui señalada?",verbose_name="Representante autorizó")
    fondo = models.ImageField(default="media/default.png",upload_to = "fondo",blank=True,null=True, help_text="Elija imagen de fondo",verbose_name="Fondo")
    fondo_resized = ImageSpecField(source='fondo',processors=[ResizeToFill(799,265)], format='PNG',options={'quality': 60})

    fondo_smart = ImageSpecField(source='fondo',processors=[SmartResize(799,265)],format='PNG')
    
    logo = models.ImageField(default="media/default.png", upload_to = "logo",blank=True,null=True, help_text="Elija imagen de logo",verbose_name="Logo")
    logo_resized = ImageSpecField(
        source='logo',processors=[ResizeToFill(150,150)], format='PNG',
        options={'quality': 60})
    
    logo_smart = ImageSpecField(
        source='logo',processors=[SmartResize(150,150)],format='PNG')
    STATUSES = (
        ("A", "Activo"),
        ("R", "Revisión"),
        ("B", "Borrador"),
    )
    status = models.CharField(max_length=1,
                                choices=STATUSES)
    def sinergias_exitosas(self):
        return self.sinergia_set.filter(status = "E")
        
    def aprobar(self):
        self.status = "A"
        self.save()

    def borrador(self):
        self.status = "B"
        self.save(update_fields=["status"]
            )

    def __unicode__(self):
        return self.nombre

    class Meta:
      ordering = ['-date_created']


class Asistencia(models.Model):
    afiliacion= models.ForeignKey(Afiliacion)
    horas= models.PositiveSmallIntegerField(help_text="Ejemplo:2",)
    fecha= models.DateField( blank=False,help_text="Ejemplo:20/10/2014",auto_now=True)


class ImpactoSocial(models.Model):
    bebes = models.IntegerField(blank=True,null=True,help_text="Número de Bebés (0 a 3 años) en su OSC",verbose_name="Bebés")
    ninas = models.IntegerField(blank=True,null=True,help_text="Número de Niñas  (4-11 años)",verbose_name="Niñas")
    ninos = models.IntegerField(blank=True,null=True,help_text="Número de Niños  (4-11 años)",verbose_name="Niños")
    jovenes = models.IntegerField(blank=True,null=True,help_text="Número de Jóvenes  (12 - 29 años)",verbose_name="Jóvenes")
    adultas = models.IntegerField(blank=True,null=True,help_text="Número de Adultas (30 - 59 años)",verbose_name="Mujeres Adultas")
    adultos = models.IntegerField(blank=True,null=True,help_text="Número de Adultos  (30 - 59 años)",verbose_name="Hombres Adultos")
    mayores_mujeres= models.IntegerField(blank=True,null=True,help_text="Número de mujeres mayores (60 años en adelante)",verbose_name="Mujeres Adultas mayores")
    mayores_hombres= models.IntegerField(blank=True,null=True,help_text="Número de hombres mayores (60 años en adelante)",verbose_name="Hombres Adultos mayores")
    colaboradores = models.IntegerField(blank=True,null=True,help_text="Número de Colaboradores en su OSC",verbose_name="Colaboradores")
    asalariados = models.IntegerField(blank=True,null=True,help_text="Número de Asalariados en su OSC",verbose_name="Asalariados")
    voluntarios = models.IntegerField(blank=True,null=True,help_text="Número de Voluntarios en su OSC",verbose_name="Voluntarios")
    fuente_ingresos = models.CharField(max_length=255,blank=True,null=True,help_text="Principal fuente de ingresos de la OSC",verbose_name="Principal fuente de ingresos")
    fuente_ingresos_FMQ = models.DecimalField(decimal_places = 2, max_digits = 999,blank=True,null=True,help_text="Expresar en %",verbose_name="Porcentaje que representa el ingreso que le brinda FMQ")
    costo_anual = models.DecimalField(decimal_places = 2, max_digits = 999,blank=True,null=True,help_text="Costo Operativo Anual",verbose_name="Costo Operativo Anual")


class NecesidadesMateriales(models.Model):
    nombre = models.CharField(max_length = 255,help_text="",verbose_name="Nombre")
    descripcion = models.CharField(max_length = 255,help_text="",verbose_name="Descripción")
   
    afiliacion = models.ForeignKey("Afiliacion")

    def __unicode__(self):
        return self.nombre

class NecesidadesHumanas(models.Model):
    nombre = models.CharField(max_length = 255)
    descripcion = models.CharField(max_length = 255,help_text="",verbose_name="Descripción")
    afiliacion = models.ForeignKey("Afiliacion")
    def __unicode__(self):
        return self.nombre

class NecesidadesCapacitacion(models.Model):
    nombre = models.CharField(max_length = 255,help_text="",verbose_name="Nombre")
    descripcion = models.CharField(max_length = 255,help_text="",verbose_name="Descripción")
  
    afiliacion = models.ForeignKey("Afiliacion")

    def __unicode__(self):
        return self.nombre

class Servicios(models.Model):
    nombre = models.CharField(max_length = 255,help_text="",verbose_name="Nombre")
    descripcion = models.CharField(max_length = 255,help_text="",verbose_name="Descripción")
    
    afiliacion = models.ForeignKey("Afiliacion")

    def __unicode__(self):
        return self.nombre

class Categoria(models.Model):
    nombre = models.CharField(max_length = 255,help_text="",verbose_name="Nombre")


    def __unicode__(self):
        return self.nombre
