# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'HistoricalAfiliacion.nombre_red_char'
        db.delete_column(u'afiliacion_historicalafiliacion', 'nombre_red_char')

        # Adding field 'HistoricalAfiliacion.nombre_red'
        db.add_column(u'afiliacion_historicalafiliacion', 'nombre_red',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Afiliacion.nombre_red_char'
        db.delete_column(u'afiliacion_afiliacion', 'nombre_red_char')

        # Adding field 'Afiliacion.nombre_red'
        db.add_column(u'afiliacion_afiliacion', 'nombre_red',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'HistoricalAfiliacion.nombre_red_char'
        db.add_column(u'afiliacion_historicalafiliacion', 'nombre_red_char',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'HistoricalAfiliacion.nombre_red'
        db.delete_column(u'afiliacion_historicalafiliacion', 'nombre_red')

        # Adding field 'Afiliacion.nombre_red_char'
        db.add_column(u'afiliacion_afiliacion', 'nombre_red_char',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Afiliacion.nombre_red'
        db.delete_column(u'afiliacion_afiliacion', 'nombre_red')


    models = {
        u'afiliacion.afiliacion': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Afiliacion'},
            'actividades': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'ano_constitucion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cargo_responsable': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cluni': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'convenio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_edited': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'distintivo_y_cemfi': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'domicilio_legal': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'donatoria_internacional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'donatoria_nacional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'email_responsable': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'esta_red': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'estado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Estado']", 'null': 'True', 'blank': 'True'}),
            'facebook': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'figura': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'fondo': ('django.db.models.fields.files.ImageField', [], {'default': "'media/default.png'", 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'impacto_social': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['afiliacion.ImpactoSocial']", 'unique': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'default': "'media/default.png'", 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mision': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'municipio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Municipio']", 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'nombre_red': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'presidente': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'representante_autirizo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'rfc': ('localflavor.mx.models.MXRFCField', [], {'max_length': '13', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'vision': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'afiliacion.asistencia': {
            'Meta': {'object_name': 'Asistencia'},
            'afiliacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Afiliacion']"}),
            'fecha': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'horas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'afiliacion.categoria': {
            'Meta': {'object_name': 'Categoria'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'afiliacion.estado': {
            'Meta': {'object_name': 'Estado'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'afiliacion.historicalafiliacion': {
            'Meta': {'ordering': "(u'-history_date', u'-history_id')", 'object_name': 'HistoricalAfiliacion'},
            'actividades': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'ano_constitucion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cargo_responsable': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cluni': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'convenio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'date_edited': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'distintivo_y_cemfi': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'domicilio_legal': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'donatoria_internacional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'donatoria_nacional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'email_responsable': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'esta_red': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'estado_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'facebook': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'figura': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'fondo': ('django.db.models.fields.TextField', [], {'default': "'media/default.png'", 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'history_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'history_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'history_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'history_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True'}),
            u'id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'blank': 'True'}),
            'impacto_social_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'logo': ('django.db.models.fields.TextField', [], {'default': "'media/default.png'", 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mision': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'municipio_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'nombre_red': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'presidente': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'representante_autirizo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'rfc': ('localflavor.mx.models.MXRFCField', [], {'max_length': '13', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'tematica': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'vision': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'afiliacion.impactosocial': {
            'Meta': {'object_name': 'ImpactoSocial'},
            'adultas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'adultos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'asalariados': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'bebes': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'colaboradores': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'costo_anual': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '999', 'decimal_places': '2', 'blank': 'True'}),
            'fuente_ingresos': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'fuente_ingresos_FMQ': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '999', 'decimal_places': '2', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jovenes': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'mayores_hombres': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'mayores_mujeres': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ninas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ninos': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'voluntarios': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'afiliacion.municipio': {
            'Meta': {'object_name': 'Municipio'},
            'estado': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Estado']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'afiliacion.necesidadescapacitacion': {
            'Meta': {'object_name': 'NecesidadesCapacitacion'},
            'afiliacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Afiliacion']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'afiliacion.necesidadeshumanas': {
            'Meta': {'object_name': 'NecesidadesHumanas'},
            'afiliacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Afiliacion']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'afiliacion.necesidadesmateriales': {
            'Meta': {'object_name': 'NecesidadesMateriales'},
            'afiliacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Afiliacion']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'afiliacion.servicios': {
            'Meta': {'object_name': 'Servicios'},
            'afiliacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['afiliacion.Afiliacion']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['afiliacion']