from django.test import TestCase
from afiliacion.models import *
from datetime import datetime, date
from django.db import models
from . import utils
import pytz
from django.contrib.auth.models import User, check_password

class DashboardViewsTestCase(TestCase):
    def test_index_dashboard(self):
        resp = self.client.get('/dashboard/')
        self.assertEqual(resp.status_code, 200)
        
    
    def test_login(self):
        resp = self.client.get('/dashboard/login')
        self.assertEqual(resp.status_code, 200)
        
    def test_register(self):
        resp = self.client.get('/dashboard/register')
        self.assertEqual(resp.status_code, 200)
        
    
    def test_index(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)
        
        
class AfiliacionCase(TestCase):
    fixtures = ['data_prueba']
    
    
    def setUp(self):
        super(AfiliacionCase, self).setUp()
        self.afiliacion_1 = Afiliacion.objects.get(pk=1)
        self.afiliacion_2 = Afiliacion.objects.get(pk=4)
        
    def test_afiliar(self):
        user = User(username="username", password="sdfds")
        user.save()
        i = ImpactoSocial()
        i.save()
        a = Afiliacion(nombre = "Funcion de Prueba", figura = "A")
        a.impacto_social  = i
        a.user = user
        a.status = "B"
        a.save()
        a.aprobar()
        self.assertTrue(a.status =="A")
        
    def test_aprobar(self):
        self.afiliacion_1.status = "B"
        self.afiliacion_1.save()
        self.afiliacion_1.aprobar()
        self.assertTrue(self.afiliacion_1.status =="A")
        
    
    def test_borrador(self):
        self.afiliacion_1.status = "B"
        self.afiliacion_1.save()
        self.afiliacion_1.borrador()
        self.assertTrue(self.afiliacion_1.status =="B")
        
    
    def test_editar_informacion(self):
        self.afiliacion_1.status = "B"
        self.afiliacion_1.actividades = "11"
        self.afiliacion_1.save()
        utc=pytz.UTC
        self.assertTrue(self.afiliacion_1.date_edited > utc.localize(datetime.now()) and self.afiliacion_1.status =="B")

class UserTests(TestCase):

    def test_generate_unique_username(self):
        examples = [('a.b-c@gmail.com', 'a.b-c'),
                    (u'username', 'username'),
                    ('User Name', 'user_name'),
                    ('', 'user')]
        for input, username in examples:
            self.assertEqual(utils.generate_unique_username([input]),
                             username)

    def test_email_validation(self):
        s = 'unfortunately.django.user.email.max_length.is.set.to.75.which.is.too.short@bummer.com'
        self.assertEqual(None, utils.valid_email_or_none(s))
        s = 'this.email.address.is.a.bit.too.long.but.should.still.validate.ok@short.com'
        self.assertEqual(s, utils.valid_email_or_none(s))
        s = 'x' + s
        self.assertEqual(None, utils.valid_email_or_none(s))
        self.assertEqual(None, utils.valid_email_or_none("Bad ?"))

    def test_serializer(self):
        class SomeModel(models.Model):
            dt = models.DateTimeField()
            t = models.TimeField()
            d = models.DateField()
        instance = SomeModel(dt=datetime.now(),
                             d=date.today(),
                             t=datetime.now().time())
        instance.nonfield = 'hello'
        data = utils.serialize_instance(instance)
        instance2 = utils.deserialize_instance(SomeModel, data)
        self.assertEqual(instance.nonfield, instance2.nonfield)
        self.assertEqual(instance.d, instance2.d)
        self.assertEqual(instance.dt.date(), instance2.dt.date())
        for t1, t2 in [(instance.t, instance2.t),
                       (instance.dt.time(), instance2.dt.time())]:
            self.assertEqual(t1.hour, t2.hour)
            self.assertEqual(t1.minute, t2.minute)
            self.assertEqual(t1.second, t2.second)
            self.assertEqual(int(t1.microsecond / 1000),
                             int(t2.microsecond / 1000))