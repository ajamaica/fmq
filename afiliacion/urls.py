from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fmq.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^$', 'afiliacion.views.dashboard',name='dashboard'),
    url(r'^notificaciones$', 'afiliacion.views.notificaciones',name='notificaciones'),
    url(r'^sinergias$', 'afiliacion.views.sinergias',name='sinergias'),
    url(r'^afiliacion$', 'afiliacion.views.afiliacion',name='afiliacion'),
    url(r'^guidelines2/$', 'afiliacion.views.guidelines2',name='guidelines2'),
    url(r'^editar_informacion/$','afiliacion.views.editar_informacion',name='editar_informacion'),
    url(r'^osc/(\d+)$', 'afiliacion.views.mostrar_osc_informacion',name = 'mostrar_osc_informacion'),
    url(r'^asistencias$', 'afiliacion.views.asistenciasLista',name = 'asistenciasLista'),
    url(r'^asistencias_asignacion/(?P<osc_id>\d+)/$', 'afiliacion.views.asignar_asistencia',name = 'asignar_asistencia'),
    url(r'^reportes$', 'afiliacion.views.reporte_lista',name = 'reporte_lista'),
    url(r'^generar_reporte$','afiliacion.views.generar_reporte',name='generar_reporte'),
    url(r'^asistencias/borrar_asignacion_lista/(?P<osc_id>\d+)/$','afiliacion.views.borrar_asignacion_lista',name='borrar_asignacion_lista'),
    url(r'^remover_necesidades$','afiliacion.views.remover_necesidades',name='remover_necesidades'),
    url(r'^afiliados$','afiliacion.views.afiliados_mejorado',name='afiliados_mejorado'),
    url(r'^lista_oscs_sinergia$','afiliacion.views.lista_oscs_sinergia',name='lista_oscs_sinergia'),
    url(r'^sinergias_osc/(?P<osc_id>\d+)/$','afiliacion.views.sinergias_osc',name='sinergias_osc'),
        
)
