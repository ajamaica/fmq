#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response,redirect
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.forms.models import inlineformset_factory
from django.contrib.admin.views.decorators import staff_member_required
from afiliacion.forms import *
from django.shortcuts import get_list_or_404, get_object_or_404
from sinergia.models import *
from sinergia.forms import *
import datetime
from dateutil.relativedelta import relativedelta
import pusher
from django.conf import settings
from django.contrib.admin.models import LogEntry, ADDITION
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger 
from django.db.models import Count, Min, Sum, Avg
import base64
import time
import csv
from django.utils.encoding import smart_str
from allauth.account.views import *

# Create your views here.

class MyPasswordChangeView(PasswordChangeView):
    success_url = "/dashboard/"

def numero_sinergia_mes(sc, today):
    p1 = today.year
    p2 = today.month
    return sc.filter(date_created__year=p1, date_created__month=p2)


def numero_afiliacion_mes(aa, today):
    p1 = today.year
    p2 = today.month
    return aa.filter(date_created__year=p1, date_created__month=p2)


def porcentaje_afiliacion(today, aa, val, number_afiliation_month):
    past_month_afiliacion = numero_afiliacion_mes(aa, val)
    value = number_afiliation_month.count() - past_month_afiliacion.count()
    if past_month_afiliacion.count() == 0:
        return value * 100
    else:
        return value / past_month_afiliacion.count() * 100


def porcentaje_sinergia(today, sinergia_completa, val, number_sinergia_month):
    past_month_sinergia = numero_sinergia_mes(sinergia_completa, val)
    value_s = number_sinergia_month.count() - past_month_sinergia.count()
    if past_month_sinergia.count() == 0:
        return value_s * 100
    else:
        return value_s / past_month_sinergia.count() * 100

def numero_asistencias_month(am,today):
    p1 = today.year
    p2 = today.month
    return am.filter(fecha__year=p1, fecha__month=p2)

def porcentaje_asistencia(today,asistencias,val,number_asistance_month):
    past_month_asistencia = numero_asistencias_month(asistencias, val)
    value_s = number_asistance_month.count() - past_month_asistencia.count()
    if past_month_asistencia.count() == 0:
        return value_s * 100
    else:
        return value_s / past_month_asistencia.count() * 100

@login_required
def notificaciones(request):
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:50]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:50]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')
    template = "notificaciones.html"
    instance = RequestContext(request, locals())
    return render_to_response(template, context_instance=instance)


@login_required
def dashboard(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')


    afiliaciones_all = Afiliacion.objects.all()

    afiliaciones_activas = afiliaciones_all.filter(status='A')
    afiliaciones_borrador= afiliaciones_all.filter(status='B')
    afiliaciones_revision= afiliaciones_all.filter(status='R')

    afiliaciones_recientes =Afiliacion.objects.all().order_by('-pk')
    afiliaciones_recientes_lista = list(afiliaciones_recientes)[:30]

    
    notificacion_sinergia_recientes= list(notificacion_sinergia)[:30]

    sinergia_completa = Sinergia.objects.filter(status='E')

###################### Codigo Raul
    if request.user.is_superuser or request.user.is_staff:

        sinergia_completa = Sinergia.objects.filter(status='E')
        sinergia_rechazada = Sinergia.objects.filter(status='F')
        sinergia_nueva = Sinergia.objects.filter(status='N')

    else:
        osc_usuario = Afiliacion.objects.filter(user=request.user)
        sinergia_completa=Sinergia.objects.filter(status='E',osc=osc_usuario)
        sinergia_rechazada = Sinergia.objects.filter(status='F',osc=osc_usuario)
        sinergia_nueva = Sinergia.objects.filter(status='N',osc=osc_usuario)
        


###################### Codigo Raul Fin
    afiliacion = Afiliacion.objects.filter(user=request.user)

    today = datetime.date.today()
    val = (today - relativedelta(months=1))

    #history_afiliation = Afiliacion.history.all()

    #################### Afiliaciones 

    number_afiliation_month = numero_afiliacion_mes(afiliaciones_activas, today)

    percentage_afiliacion = porcentaje_afiliacion(today, afiliaciones_activas, val, number_afiliation_month)


    #############Sinergias

    number_sinergia_month = numero_sinergia_mes(sinergia_completa, today)

    #n = number_sinergia_month

    percentage_sinergia = porcentaje_sinergia(today, sinergia_completa, val, number_sinergia_month)

    ############### Asistencias

    asistencias_totales = Asistencia.objects.all()

    number_asistences_month = numero_asistencias_month(asistencias_totales,today)

    asistencias = Asistencia.objects.all()

    percentage_asistencias = porcentaje_asistencia(today, asistencias, val, number_asistences_month)

    #################################


    if not (request.user.is_superuser or request.user.is_staff):
        if not afiliacion:
            return HttpResponseRedirect('/dashboard/afiliacion')
        afiliacion = afiliacion[0]
        if afiliacion.status != "A" :
          if "success" in request.GET:
            return HttpResponseRedirect('/dashboard/editar_informacion?success=2')
          else:
            return HttpResponseRedirect('/dashboard/editar_informacion')
    template = "dashboard.html"
    instance = RequestContext(request, locals())
    return render_to_response(template, context_instance=instance)


@login_required
@staff_member_required
def mostrar_osc_informacion(request, pk):
    afiliacion = get_object_or_404(Afiliacion, pk=pk)
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    if request.method == 'POST':
        if "AprobarM" in request.POST:
            afiliacion.aprobar()

            subject = request.POST['asunto']
            p_begin = '<p align="justify">'
            mensaje = request.POST['mensaje']
            p_end = "</p>"
            mensaje = mensaje.replace('\n', '<br>')
            mensaje = p_begin + mensaje + p_end
            fr = "contacto@fundacionmercedqro.com"
            t = afiliacion.user.email
            mensaje = EmailMessage(subject, mensaje, fr, [t, ])
            mensaje.content_subtype = "html"  # Main content is now text/html
            mensaje.send()

            success = 1
            success = str(success)
            return HttpResponseRedirect('/dashboard/?successful=' + success)
        elif "CancelarM" in request.POST:
            afiliacion.borrador()
            
            subject = request.POST['asunto']
            sitio_link = '<a href="'+settings.URL+'/dashboard/editar_informacion/">Ir al borrador</a>'
            p_begin = '<p align="justify">'
            mensaje = request.POST['mensaje']
            p_end = "</p>"
            mensaje = mensaje.replace('\n', '<br>')
            mensaje = mensaje.replace('<sitio_link>', sitio_link)
            mensaje = p_begin + mensaje + p_end
            fr = "contacto@fundacionmercedqro.com"
            t = afiliacion.user.email
            mensaje = EmailMessage(subject, mensaje, fr, [t, ])
            mensaje.content_subtype = "html"  # Main content is now text/html
            mensaje.send()

            success = 0
            success = str(success)
            return HttpResponseRedirect('/dashboard/?successful=' + success)
        else:
            i = {
                'afiliacion': afiliacion, 
                'impacto_social': afiliacion.impacto_social
                }
            form = AfiliacionMultiForm(request.POST, request.FILES, instance=i)
            qs = NecesidadesMateriales.objects.filter(afiliacion=afiliacion)
            formset = NecesidadesMaterialesFormset(
                request.POST, 
                request.FILES, 
                queryset=qs,
                prefix="formset"
                )
            qs = NecesidadesHumanas.objects.filter(afiliacion=afiliacion)
            formset2 = NecesidadesHumanasFormset(
                request.POST, 
                request.FILES, 
                prefix="formset2",
                queryset=qs
                )
            qs = NecesidadesCapacitacion.objects.filter(afiliacion=afiliacion)
            formset3 = NecesidadesCapacitacionset(
                request.POST,
                request.FILES,
                prefix="formset3",
                queryset=qs
                )
            formset4 = ServiciosFormset(
                request.POST,
                request.FILES, 
                prefix="formset4", 
                queryset=Servicios.objects.filter(afiliacion=afiliacion)
                )
            if form.is_valid() and formset.is_valid() and formset2.is_valid() and formset3.is_valid() and formset4.is_valid():
                if 'Aprobar' in request.POST or 'GUARDADO' in request.POST:
                    afiliacion.aprobar()
                    ambas = form.save(commit=False)
                    afiliacion = ambas["afiliacion"]
                    impacto_social = ambas["impacto_social"]
                    impacto_social.save()
                    afiliacion.impacto_social = impacto_social
                    afiliacion.user = request.user
                    for obj in formset.save(commit=False):
                        obj.afiliacion = afiliacion
                        obj.save()

                    for obj in formset2.save(commit=False):
                        obj.afiliacion = afiliacion
                        obj.save()

                    for obj in formset3.save(commit=False):
                        obj.afiliacion = afiliacion
                        obj.save()

                    for obj in formset4.save(commit=False):
                        obj.afiliacion = afiliacion
                        obj.save()

                    success = 1
                elif 'Cancelar' in request.POST:
                    afiliacion.borrador()
                    success = 0
                    s = str(success)
                    return HttpResponseRedirect('/dashboard/?successful=' + s)
    else:
        form = AfiliacionMultiForm(
            instance={
                'afiliacion': afiliacion, 
                'impacto_social': afiliacion.impacto_social
                }
            )
        q = NecesidadesMateriales.objects.filter(afiliacion=afiliacion)
        formset = NecesidadesMaterialesFormset(
            prefix="formset",
            queryset=q)
        q = NecesidadesHumanas.objects.filter(afiliacion=afiliacion)
        formset2 = NecesidadesHumanasFormset(
            prefix="formset2",
            queryset=q)
        q = NecesidadesCapacitacion.objects.filter(afiliacion=afiliacion)
        formset3 = NecesidadesCapacitacionset(
            prefix="formset3",
            queryset=q)
        formset4 = ServiciosFormset(
            prefix="formset4",
            queryset=Servicios.objects.filter(afiliacion=afiliacion))

    estados = Estado.objects.all().order_by('nombre')
    municipios = []

    for e in estados:
        municipios.append(Municipio.objects.filter(estado=e.id).order_by('nombre'))



    template = "afiliacion.html"
    instance = RequestContext(request, locals())
    return render_to_response(template, context_instance=instance)


@login_required
def editar_informacion(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    afiliacion = Afiliacion.objects.filter(user=request.user)
    if not afiliacion:
        return HttpResponseRedirect('/dashboard/afiliacion')
    afiliacion = afiliacion[0]

    if afiliacion.status == "R":
        template = "enrevision.html"
        instance = RequestContext(request, locals())
        return render_to_response(template, context_instance=instance)
    else:

        if request.method == 'POST':
          form = AfiliacionMultiForm(request.POST, request.FILES,instance={'afiliacion': afiliacion,'impacto_social': afiliacion.impacto_social})

          formset = NecesidadesMaterialesFormset(request.POST, request.FILES, queryset=NecesidadesMateriales.objects.filter(afiliacion  = afiliacion),  prefix="formset")

          formset2 = NecesidadesHumanasFormset(request.POST, request.FILES, prefix="formset2",queryset= NecesidadesHumanas.objects.filter(afiliacion  = afiliacion))

          formset3 = NecesidadesCapacitacionset(request.POST, request.FILES, prefix="formset3",queryset= NecesidadesCapacitacion.objects.filter(afiliacion  = afiliacion))

          formset4 = ServiciosFormset(request.POST, request.FILES, prefix="formset4", queryset= Servicios.objects.filter(afiliacion  = afiliacion))

          if form.is_valid() and formset.is_valid() and formset2.is_valid() and formset3.is_valid() and formset4.is_valid():
              ambas = form.save(commit = False)
              afiliacion = ambas["afiliacion"]
              impacto_social = ambas["impacto_social"]
              impacto_social.save()
              afiliacion.impacto_social = impacto_social
              afiliacion.user = request.user

              if 'Borrador' in request.POST:
                  afiliacion_borrador = 2
                  afiliacion.borrador()
                  
              elif 'Revision' in request.POST:
                  afiliacion.status = "R"
        if request.method == 'POST':
            form = AfiliacionMultiForm(
                request.POST, 
                request.FILES,
                instance={
                    'afiliacion': afiliacion,
                    'impacto_social': afiliacion.impacto_social})

            formset = NecesidadesMaterialesFormset(
                request.POST, 
                request.FILES, 
                queryset=NecesidadesMateriales.objects.filter(
                    afiliacion=afiliacion),
                prefix="formset")

            formset2 = NecesidadesHumanasFormset(
                request.POST,
                request.FILES,
                prefix="formset2",
                queryset=NecesidadesHumanas.objects.filter(
                    afiliacion=afiliacion))

            formset3 = NecesidadesCapacitacionset(
                request.POST,
                request.FILES,
                prefix="formset3",
                queryset=NecesidadesCapacitacion.objects.filter(
                    afiliacion=afiliacion))

            formset4 = ServiciosFormset(
                request.POST,
                request.FILES,
                prefix="formset4",
                queryset=Servicios.objects.filter(afiliacion=afiliacion))

            if form.is_valid() and formset.is_valid() and formset2.is_valid() and formset3.is_valid() and formset4.is_valid():
                ambas = form.save(commit=False)
                afiliacion = ambas["afiliacion"]
                impacto_social = ambas["impacto_social"]
                impacto_social.save()
                afiliacion.impacto_social = impacto_social
                afiliacion.user = request.user

                if 'Borrador' in request.POST:
                    afiliacion.borrador()

                elif 'Revision' in request.POST:
                    afiliacion.status = "R"

                ####### Codigo Raul
                if 'Editar' in request.POST:
                    afiliacion.aprobar()

                ####### Codigo Raul

                p = pusher.Pusher(
                    app_id='93096',
                    key='e0918b342f61eed510c3',
                    secret='e743868b97ea68ace31a')
                qset = Afiliacion.objects.filter(status='R')
                val = qset.count() + 1
                p['test_channel'].trigger(
                    'my_event', 
                    {
                        'nombre_afiliacion': afiliacion.nombre,
                        'url': '/dashboard/osc/'+str(afiliacion.id),
                        'numero': str(val)})

                afiliacion.save()

                for obj in formset.save(commit=False):
                    obj.afiliacion = afiliacion
                    obj.save()

                for obj in formset2.save(commit=False):
                    obj.afiliacion = afiliacion
                    obj.save()

                for obj in formset3.save(commit=False):
                    obj.afiliacion = afiliacion
                    obj.save()

                for obj in formset4.save(commit=False):
                    obj.afiliacion = afiliacion
                    obj.save()
                
                return HttpResponseRedirect('/dashboard?success=2')

            else:

                errores = True
        else:
            otraform = EstadoForm()
            form = AfiliacionMultiForm(
                instance={
                    'afiliacion': afiliacion,
                    'impacto_social': afiliacion.impacto_social})
            formset = NecesidadesMaterialesFormset(
                prefix="formset",
                queryset=NecesidadesMateriales.objects.filter(
                    afiliacion=afiliacion))
            formset2 = NecesidadesHumanasFormset(
                prefix="formset2",
                queryset=NecesidadesHumanas.objects.filter(
                    afiliacion=afiliacion))
            formset3 = NecesidadesCapacitacionset(
                prefix="formset3",
                queryset=NecesidadesCapacitacion.objects.filter(
                    afiliacion=afiliacion))
            formset4 = ServiciosFormset(
                prefix="formset4",
                queryset=Servicios.objects.filter(afiliacion=afiliacion))

    estados = Estado.objects.all().order_by('nombre')
    municipios = []

    for e in estados:
        municipios.append(Municipio.objects.filter(estado=e.id).order_by('nombre'))

    template = "afiliacion.html"
    return render_to_response(
        template,
        context_instance=RequestContext(request, locals()))


@login_required
def afiliacion(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    afiliacion = Afiliacion.objects.filter(user=request.user)
    if afiliacion:
        afiliacion = afiliacion[0]
        if afiliacion.status != "A":
            return HttpResponseRedirect('/dashboard/editar_informacion')

    if request.method == 'POST':
        form = AfiliacionMultiForm(request.POST, request.FILES)

        formset = NecesidadesMaterialesFormset(
            request.POST,
            request.FILES,
            prefix="formset",
            queryset=NecesidadesMateriales.objects.none())

        formset2 = NecesidadesHumanasFormset(
            request.POST,
            request.FILES,
            prefix="formset2",
            queryset=NecesidadesHumanas.objects.none())

        formset3 = NecesidadesCapacitacionset(
            request.POST,
            request.FILES,
            prefix="formset3",
            queryset=NecesidadesCapacitacion.objects.none())

        formset4 = ServiciosFormset(
            request.POST,
            request.FILES,
            prefix="formset4",
            queryset=Servicios.objects.none())

        if form.is_valid() and formset.is_valid() and formset2.is_valid() and formset3.is_valid() and formset4.is_valid():
            ambas = form.save(commit=False)
            afiliacion = ambas["afiliacion"]
            impacto_social = ambas["impacto_social"]
            impacto_social.save()
            afiliacion.impacto_social = impacto_social
            afiliacion.user = request.user
            

            if 'Borrador' in request.POST:
                afiliacion.save()
                afiliacion.borrador()
                afiliacion_borrador = 2

            elif 'Revision' in request.POST:
                afiliacion.status = "R"
                afiliacion.save()

                p = pusher.Pusher(
                        app_id='93096',
                        key='e0918b342f61eed510c3',
                        secret='e743868b97ea68ace31a')
                qset = Afiliacion.objects.filter(status='R')
                val = qset.count() + 1
                p['test_channel'].trigger(
                    'my_event', 
                    {
                        'nombre_afiliacion': afiliacion.nombre,
                        'url': '/dashboard/osc/'+str(afiliacion.id),
                            'numero': str(val)})

            for obj in formset.save(commit=False):
                obj.afiliacion = afiliacion
                obj.save()

            for obj in formset2.save(commit=False):
                obj.afiliacion = afiliacion
                obj.save()

            for obj in formset3.save(commit=False):
                obj.afiliacion = afiliacion
                obj.save()

            for obj in formset4.save(commit=False):
                obj.afiliacion = afiliacion
                obj.save()

            return HttpResponseRedirect('/dashboard/afiliacion')
    else:
        otraform = EstadoForm()
        form = AfiliacionMultiForm()
        formset = NecesidadesMaterialesFormset(
            prefix="formset",
            queryset=NecesidadesMateriales.objects.none())
        formset2 = NecesidadesHumanasFormset(
            prefix="formset2",
            queryset=NecesidadesHumanas.objects.none())
        formset3 = NecesidadesCapacitacionset(
            prefix="formset3",
            queryset=NecesidadesCapacitacion.objects.none())
        formset4 = ServiciosFormset(
            prefix="formset4",
            queryset=Servicios.objects.none())


    estados = Estado.objects.all().order_by('nombre')
    municipios = []

    for e in estados:
        municipios.append(Municipio.objects.filter(estado=e.id).order_by('nombre'))

    template = "afiliacion.html"
    return render_to_response(template, context_instance=RequestContext(
        request, locals()))


def guidelines2(request):
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    afiliacion = Afiliacion.objects.filter(user=request.user)
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia = Sinergia.objects.all()
    template = "ui_general.html"
    return render_to_response(template, context_instance=RequestContext(
        request, locals()))



@login_required
def sinergias(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')
    
    number = base64.urlsafe_b64decode(request.GET.get('number', '').encode("utf-8"))
    
    if request.method == 'POST':
        if 'confirmaM' in request.POST:
            sinergiaId = request.POST['sinergia']
            sinergia = Sinergia.objects.get(id=sinergiaId)
            number = base64.urlsafe_b64encode(sinergiaId)
            subject = request.POST['asunto']
            p_begin = '<p align="justify">'
            osc_nombre = '<strong>' + sinergia.osc.nombre + '</strong>'
            sitio_link = '<a href="'+settings.URL+'">www.fundacionmerced.org</a>'
            evaluacion_link = '<a href="'+settings.URL+'/dashboard/sinergias?number='+number+'">Evaluar sinergia</a>'
            mensaje = request.POST['mensaje']
            mensaje = mensaje.replace('<osc_nombre>', osc_nombre)
            mensaje = mensaje.replace('<sitio_link>', sitio_link)
            mensaje = mensaje.replace('<evaluacion_link>', evaluacion_link)
            p_end = "</p>"
            msg = p_begin + mensaje + p_end
            fr = "contacto@fundacionmercedqro.com"
            t = sinergia.osc.user.email
            msg = EmailMessage(subject, msg, fr, [t, ])
            msg.content_subtype = "html"  # Main content is now text/html
            msg.send()
            return HttpResponseRedirect('/dashboard/sinergias')
        elif 'aceptoS' in request.POST:
          #codigo
              form_aceptosinergia = Multi_AceptoSinergia(request.POST, request.FILES)
              forma = SinergiaCancelar()
              if form_aceptosinergia.is_valid():
                  
                  numero = 2
                  sinergia_aceptada = 2

              else:
                  no = Sinergia.objects.get(id=request.POST['sin']).id
                  numero = 1

              #return HttpResponseRedirect('/dashboard/sinergias')

        else:
            forma = SinergiaCancelar(request.POST, request.FILES)
            form_aceptosinergia = Multi_AceptoSinergia()
            if forma.is_valid():
                Sinergia.objects.filter(id=request.POST['sin_rechazo']).update(comentario=request.POST['comentario'], status='F')
                sinergia_aceptada = 1

            else:
                var = Sinergia.objects.get(id=request.POST['sin_rechazo']).id
                hola = 1
    else:
        forma = SinergiaCancelar()
        form_aceptosinergia = Multi_AceptoSinergia()
          
    if request.user.is_superuser or request.user.is_staff:
        sinergia_all = Sinergia.objects.all()
    else:
        if number != '':
            sinergia_all = [Sinergia.objects.get(id=number)]
        else:
            i = Afiliacion.objects.filter(user=request.user)
            sinergia_all = Sinergia.objects.filter(osc__in=i)
############################################# Codigo Raul
    sinergia_all= sinergia_all.all().order_by('-pk')

    paginator= Paginator(sinergia_all,25)

    page = request.GET.get('page')
    
    try:
        sinergia_all = paginator.page(page)
    except PageNotAnInteger:
        sinergia_all = paginator.page(1)
    except EmptyPage:
        sinergia_all = paginator.page(paginator.num_pages)

############################################# Codigo Raul

    template = "sinergias.html"
    instance = RequestContext(request, locals())
    return render_to_response(template, context_instance=instance)


@login_required
def afiliados(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    afiliacion = Afiliacion.objects.filter(user=request.user)
    if request.user.is_superuser or request.user.is_staff:
        osc = Afiliacion.objects.all()
    else:
        osc = Afiliacion.objects.filter(user=request.user)
    template = "afiliados.html"
    return render_to_response(template, context_instance=RequestContext(
        request, locals()))

@login_required
def asistenciasLista(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    busqueda=""

    if request.method == 'POST':

        if request.POST['busqueda']=="":    
            afiliaciones_activas = Afiliacion.objects.filter(status="A")
            afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave
            page= 1
        
        else:
           # afiliaciones_activas= Afiliacion.objects.get(nombre__icontains=busqueda)
            busqueda=request.POST['busqueda']
            afiliaciones_activas= Afiliacion.objects.filter(status="A",nombre__icontains=request.POST['busqueda'])
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page=1


    else:
        if request.GET.get('busqueda2'):
            if request.GET['busqueda2']=="":
                busqueda=""
                afiliaciones_activas = Afiliacion.objects.filter(status="A")
                afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave
                page=1
            else:
                busqueda=request.GET['busqueda2']
                afiliaciones_activas= Afiliacion.objects.filter(status="A",nombre__icontains=request.GET['busqueda2'])
                afiliaciones_activas= afiliaciones_activas.order_by('nombre')
                page= request.GET.get('page')

        else:
            busqueda=""
            afiliaciones_activas= Afiliacion.objects.filter(status="A")
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page= request.GET.get('page')


    
    paginator= Paginator(afiliaciones_activas,25)
    


    try:
        afiliaciones_activas = paginator.page(page)
    except PageNotAnInteger:
        afiliaciones_activas = paginator.page(1)
    except EmptyPage:
        afiliaciones_activas = paginator.page(paginator.num_pages)


    template ="asistencias.html"

    return render_to_response(template,context_instance=RequestContext(request,locals()))

@login_required
def asignar_asistencia(request,osc_id):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    osc = Afiliacion.objects.get(pk=osc_id)
    today = datetime.date.today()
    
    if request.method == 'POST':
        
        forma=AsistenciaForm(request.POST)


        if forma.is_valid():
            osc= Afiliacion.objects.get(pk=osc_id)
            horas_form=forma.cleaned_data['horas']
            #fecha_form=forma.cleaned_data['fecha']
            capacitacion = Asistencia(afiliacion=osc,horas=horas_form)
            capacitacion.save()
            mensaje= "Asistencia Creada"
            
            # se salva la nueva asistencia
            #return HttpResponseRedirect('/dashboard/asistencias_asignacion/'+osc_id)
        
           
       
    else: ## si no es post
        forma = AsistenciaForm()

    
    template ="asistencias_asignacion.html"


    return render_to_response(template,context_instance=RequestContext(request,locals()))


@login_required
def borrar_asignacion_lista(request,osc_id):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    mensaje=""

    if request.method== 'POST':
        mensaje="Asistencia Borrada"
        asistencia_borrar= Asistencia.objects.get(pk=request.POST['borrar'])
        asistencia_borrar.delete()


    osc= Afiliacion.objects.get(pk=osc_id)
    
    asistencias = Asistencia.objects.filter(afiliacion=osc)

    asistencias_list = asistencias.order_by('-fecha')

    paginator= Paginator(asistencias_list,25)
    
    page = request.GET.get('page')

    try:
        asistencias_lista = paginator.page(page)
    except PageNotAnInteger:
        asistencias_lista = paginator.page(1)
    except EmptyPage:
        asistencias_lista = paginator.page(paginator.num_pages)




    template ="asignacion_lista_borrar.html"

    return render_to_response(template,context_instance=RequestContext(request,locals()))

    





@login_required
def reporte_lista(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    busqueda=""

    if request.method == 'POST':

        if request.POST['busqueda']=="":    
            afiliaciones_activas = Afiliacion.objects.filter(status="A")
            afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave
            page= 1
        
        else:
           # afiliaciones_activas= Afiliacion.objects.get(nombre__icontains=busqueda)
            busqueda=request.POST['busqueda']
            afiliaciones_activas= Afiliacion.objects.filter(status="A",nombre__icontains=request.POST['busqueda'])
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page=1


    else:
        if request.GET.get('busqueda2'):
            if request.GET['busqueda2']=="":
                busqueda=""
                afiliaciones_activas = Afiliacion.objects.filter(status="A")
                afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave
                page=1
            else:
                busqueda=request.GET['busqueda2']
                afiliaciones_activas= Afiliacion.objects.filter(status="A",nombre__icontains=request.GET['busqueda2'])
                afiliaciones_activas= afiliaciones_activas.order_by('nombre')
                page= request.GET.get('page')

        else:
            busqueda=""
            afiliaciones_activas= Afiliacion.objects.filter(status="A")
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page= request.GET.get('page')


    
    paginator= Paginator(afiliaciones_activas,25)
    


    try:
        afiliaciones_activas = paginator.page(page)
    except PageNotAnInteger:
        afiliaciones_activas = paginator.page(1)
    except EmptyPage:
        afiliaciones_activas = paginator.page(paginator.num_pages)


    template ="reportes_lista.html"

    return render_to_response(template,context_instance=RequestContext(request,locals()))




@login_required
def generar_reporte(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    lista_oscs_ids = request.GET.getlist("osc")

    lista_oscs =  Afiliacion.objects.filter(pk__in=lista_oscs_ids).order_by('nombre')

    if request.GET['fecha_inicio']=="" or request.GET['fecha_fin']=="":
        error_fecha_vacia=True
        template = "reporte_generar.html"
        return render_to_response(template,context_instance=RequestContext(request,locals()))


    fecha_tabla_inicio= request.GET['fecha_inicio']
    fecha_tabla_fin= request.GET['fecha_fin']

    fecha_inicio = request.GET['fecha_inicio']
    fecha_inicio_lista = fecha_inicio.split("-")
    fecha_inicio_anio= int(fecha_inicio_lista[0])
    fecha_inicio_mes= int(fecha_inicio_lista[1])
    fecha_inicio_dia= int(fecha_inicio_lista[2])
        

    fecha_fin = request.GET['fecha_fin']
    fecha_fin_lista = fecha_fin.split("-")
    fecha_fin_anio= int(fecha_fin_lista[0])
    fecha_fin_mes= int(fecha_fin_lista[1])
    fecha_fin_dia= int(fecha_fin_lista[2])
      
    fecha_inicio_parametro = datetime.date(fecha_inicio_anio,fecha_inicio_mes,fecha_inicio_dia)
    fecha_inicio_parametro-= datetime.timedelta(days=1)

    fecha_fin_parametro = datetime.date(fecha_fin_anio,fecha_fin_mes,fecha_fin_dia)
    fecha_fin_parametro += datetime.timedelta(days=1)


    if fecha_inicio_parametro >= fecha_fin_parametro:

        error_fecha=True
        template = "reporte_generar.html"
        return render_to_response(template,context_instance=RequestContext(request,locals()))


    horas_osc = list()

    for osc in lista_oscs:
        valor_horas = 0
        asistencias = Asistencia.objects.filter(afiliacion =osc)
        asistenciasPrueba = Asistencia.objects.filter(afiliacion =osc,fecha__gt=fecha_inicio_parametro,fecha__lt=fecha_fin_parametro)


        for horas in asistenciasPrueba:
            valor_horas += horas.horas

        horasux = HorasAux(total_horas = valor_horas,osc =osc.nombre)
        horas_osc.append(horasux)

    if request.GET.getlist("excel"):
        import xlwt
        
        response = HttpResponse(mimetype='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=reporte.xls'
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet("Horas")
        
        ws.write(0,0, "Rango de fechas")
        ws.write(0,1, request.GET["fecha_inicio"])
        ws.write(0,2,request.GET["fecha_fin"])
        

        for num in xrange(len(horas_osc)) :
            ws.write(num +1 , 0, horas_osc[num].osc)
            ws.write(num +1 , 1, horas_osc[num].total_horas)
            
        wb.save(response)
        return response

    
    template = "reporte_generar.html"
    
    return render_to_response(template,context_instance=RequestContext(request,locals()))

    

@login_required
def remover_necesidades(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    if request.POST:

        if 'necesidad_material' in request.POST:
            
            necesidades_materiales_borrar_ids = request.POST.getlist("necesidad_material")

            necesidades_a_borrar=NecesidadesMateriales.objects.filter(pk__in=necesidades_materiales_borrar_ids)

            necesidades_a_borrar.delete()

        if 'necesidad_humana' in request.POST:
            
            necesidades_humanas_borrar_ids = request.POST.getlist("necesidad_humana")

            necesidades_a_borrar=NecesidadesHumanas.objects.filter(pk__in=necesidades_humanas_borrar_ids)

            necesidades_a_borrar.delete()

        if 'necesidad_capacitacion' in request.POST:
            
            necesidades_capacitacion_borrar_ids = request.POST.getlist("necesidad_capacitacion")

            necesidades_a_borrar=NecesidadesCapacitacion.objects.filter(pk__in=necesidades_capacitacion_borrar_ids)

            necesidades_a_borrar.delete()

            

    osc = Afiliacion.objects.filter(user=request.user)
    necesidades_materiales= NecesidadesMateriales.objects.filter(afiliacion = osc)
    necesidades_humanas= NecesidadesHumanas.objects.filter(afiliacion=osc)
    necesidades_capacitacion = NecesidadesCapacitacion.objects.filter(afiliacion=osc)

    template = "remover_necesidad.html"
    return render_to_response(template,context_instance=RequestContext(request,locals()))




@login_required
def afiliados_mejorado(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    busqueda=""
    estatus2="T"
    

    if request.method == 'POST':

        estatus2= request.POST['estado_afiliacion'];

        if request.POST['busqueda']=="":    

            if request.POST['estado_afiliacion']=="T":
                afiliaciones_activas = Afiliacion.objects.all()
                afiliaciones_activas = afiliaciones_activas.order_by('nombre') 
                page= 1

            else:
                afiliaciones_activas = Afiliacion.objects.filter(status=request.POST['estado_afiliacion'],nombre__icontains=request.POST['busqueda'])
                afiliaciones_activas = afiliaciones_activas.order_by('nombre') 
                page= 1
        
        else:

            if request.POST['estado_afiliacion']=="T":

                afiliaciones_activas= Afiliacion.objects.filter(nombre__icontains=request.POST['busqueda'])
                afiliaciones_activas= afiliaciones_activas.order_by('nombre')
                page=1

            else:
                busqueda=request.POST['busqueda']
                afiliaciones_activas= Afiliacion.objects.filter(status=request.POST['estado_afiliacion'],nombre__icontains=request.POST['busqueda'])
                afiliaciones_activas= afiliaciones_activas.order_by('nombre')
                page=1


    else:

        debug= 1

        if 'busqueda2' in request.GET:
            
            estatus2=request.GET['estatus']

            debug= 2

            if request.GET['busqueda2']=="":

                busqueda=""

                if estatus2=="T":

                    afiliaciones_activas = Afiliacion.objects.all()
                    afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave

                else:
                    
                    estatus2=request.GET['estatus']
                    afiliaciones_activas = Afiliacion.objects.filter(status=estatus2)
                    afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave

                page=request.GET.get('page') #1

            else:

                busqueda=request.GET['busqueda2']

                if estatus2=="T":

                    afiliaciones_activas= Afiliacion.objects.filter(nombre__icontains=request.GET['busqueda2'])
                    afiliaciones_activas= afiliaciones_activas.order_by('nombre')
                else:
                    entre=" Entre"
                    estatus2=request.GET['estatus']
                    afiliaciones_activas= Afiliacion.objects.filter(status=estatus2,nombre__icontains=request.GET['busqueda2'])
                    afiliaciones_activas= afiliaciones_activas.order_by('nombre')

                
                page= request.GET.get('page')

        else:
            busqueda=""
            afiliaciones_activas= Afiliacion.objects.all()
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page= request.GET.get('page')


    
    paginator= Paginator(afiliaciones_activas,25)
    


    try:
        afiliaciones_activas = paginator.page(page)
    except PageNotAnInteger:
        afiliaciones_activas = paginator.page(1)
    except EmptyPage:
        afiliaciones_activas = paginator.page(paginator.num_pages)


    template ="afiliados_mejorado.html"

    return render_to_response(template,context_instance=RequestContext(request,locals()))


@login_required
def lista_oscs_sinergia(request):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')

    busqueda=""

    if request.method == 'POST':

        if request.POST['busqueda']=="":    
            afiliaciones_activas = Afiliacion.objects.filter(status="A")
            afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave
            page= 1
        
        else:
           # afiliaciones_activas= Afiliacion.objects.get(nombre__icontains=busqueda)
            busqueda=request.POST['busqueda']
            afiliaciones_activas= Afiliacion.objects.filter(status="A",nombre__icontains=request.POST['busqueda'])
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page=1


    else:
        if request.GET.get('busqueda2'):
            if request.GET['busqueda2']=="":
                busqueda=""
                afiliaciones_activas = Afiliacion.objects.filter(status="A")
                afiliaciones_activas = afiliaciones_activas.order_by('nombre') #clave
                page=1
            else:
                busqueda=request.GET['busqueda2']
                afiliaciones_activas= Afiliacion.objects.filter(status="A",nombre__icontains=request.GET['busqueda2'])
                afiliaciones_activas= afiliaciones_activas.order_by('nombre')
                page= request.GET.get('page')

        else:
            busqueda=""
            afiliaciones_activas= Afiliacion.objects.filter(status="A")
            afiliaciones_activas= afiliaciones_activas.order_by('nombre')
            page= request.GET.get('page')


    
    paginator= Paginator(afiliaciones_activas,25)
    


    try:
        afiliaciones_activas = paginator.page(page)
    except PageNotAnInteger:
        afiliaciones_activas = paginator.page(1)
    except EmptyPage:
        afiliaciones_activas = paginator.page(paginator.num_pages)


    template ="lista_oscs_sinergia.html"

    return render_to_response(template,context_instance=RequestContext(request,locals()))





@login_required
def sinergias_osc(request,osc_id):
    #notificaciones
    notificacion_afiliacion = Afiliacion.objects.filter(status="R")
    notificacion_sinergia = Sinergia.objects.all()[:25]
    notificacion_sinergia_campana = Sinergia.objects.filter(status="N").order_by('-pk')
    notificacion_usuario = Afiliacion.objects.filter(user=request.user)
    notificacion_sinergia_usuario = Sinergia.objects.filter(osc__in=notificacion_usuario)[:25]
    notificacion_sinergia_usuario_campana = Sinergia.objects.filter(osc__in=notificacion_usuario).filter(status="N").order_by('-pk')
    number = base64.urlsafe_b64decode(request.GET.get('number', '').encode("utf-8"))

    ############Busqueda
    busqueda=""
    estatus2="N"
    
    ############Busqueda

   
    
    if request.method == 'POST':

        if 'id_sinergia_editar' in request.POST:

            sinergia_editar =  Sinergia.objects.get(id= request.POST['id_sinergia_editar'])

            sinergia_editar.status='N'
            sinergia_editar.comentario = ''
            sinergia_editar.comentario_empresa = ''
            sinergia_editar.save()
            sinergia_editar.empresa.url = ''
            sinergia_editar.empresa.logo = 'media/default.png'
            sinergia_editar.empresa.save()

        if 'busqueda' in request.POST: # Si busqueda esta en post significa que debemos de buscar

            estatus2= request.POST['estado_sinergia'];
            

            if request.POST['busqueda']=="":

                if request.POST['estado_sinergia']=="T":
                    sinergias_osc= Sinergia.objects.all().filter(osc=osc_id)
                else:    
                    sinergias_osc = Sinergia.objects.all().filter(osc=osc_id,status=request.POST['estado_sinergia'])

                sinergias_osc = sinergias_osc.order_by('-pk') 
                page= 1
            
            else:
                busqueda=request.POST['busqueda']

                if request.POST['estado_sinergia']=="T":
                    empresas = Empresa.objects.filter(nombre__icontains=request.POST['busqueda'])
                    sinergias_osc= Sinergia.objects.filter(osc=osc_id,empresa=empresas)
                else:
                    empresas = Empresa.objects.filter(nombre__icontains=request.POST['busqueda'])
                    sinergias_osc= Sinergia.objects.filter(osc=osc_id,status=request.POST['estado_sinergia'],empresa=empresas)

                sinergias_osc= sinergias_osc.order_by('-pk')
                page=1 

        
##########################################################################################################################################################################
        else:    # Checa que tipo de POST es, si para hacer una busqueda o aprovar, rechazar o enviar mail 

            sinergias_osc = Sinergia.objects.all().filter(osc=osc_id,status=estatus2) #codigo raul
            sinergias_osc = sinergias_osc.order_by('-pk')#codigo raul
            page= 1#Codigo raul

            if 'confirmaM' in request.POST:      # Enviar Email
                
                sinergiaId = request.POST['sinergia']
                sinergia = Sinergia.objects.get(id=sinergiaId)
                number = base64.urlsafe_b64encode(sinergiaId)
                subject = request.POST['asunto']
                p_begin = '<p align="justify">'
                osc_nombre = '<strong>' + sinergia.osc.nombre + '</strong>'
                sitio_link = '<a href="' + settings.URL + '">www.fundacionmerced.org</a>'
                evaluacion_link = '<a href="' + settings.URL + '/dashboard/sinergias_osc/' + osc_id + '?number='+number+'">Evaluar sinergia</a>'
                mensaje = request.POST['mensaje']
                mensaje = mensaje.replace('<osc_nombre>', osc_nombre)
                mensaje = mensaje.replace('<sitio_link>', sitio_link)
                mensaje = mensaje.replace('<evaluacion_link>', evaluacion_link)
                mensaje = mensaje.replace('\n', '<br>')
                p_end = "</p>"
                msg = p_begin + mensaje + p_end
                fr = "contacto@fundacionmercedqro.com"
                t = sinergia.osc.user.email
                msg = EmailMessage(subject, msg, fr, [t, ])
                msg.content_subtype = "html"  # Main content is now text/html
                msg.send()

                return HttpResponseRedirect('/dashboard/sinergias_osc/'+ osc_id)

            elif 'aceptoS' in request.POST:  #Aceptar Sinergia
              #codigo
                #sacar la instancia de la sinergia YA existente.
                sinergia_actual = Sinergia.objects.filter(id=request.POST['sin'])
                sinergia_actual = sinergia_actual[0]

                #indicar a la forma que no se guardaron datos x primera vez. Sino que se hara un UPDATE de la sinergia YA existente
                form_aceptosinergia = AceptoSinergia_Form2(request.POST,instance=sinergia_actual)
                forma = SinergiaCancelar()

                if form_aceptosinergia.is_valid():
                    f_sinergia = form_aceptosinergia.save(commit=False)                                   
                    
                    f_sinergia.status = 'E'
                    f_sinergia.save()
                    
                    numero = 2
                    sinergia_aceptada = 2

                    sinergia = sinergia_actual
                    subject = request.POST['asunto']
                    p_begin = '<p align="justify">'
                    osc_nombre = '<strong>' + sinergia.osc.nombre + '</strong>'  #o --> sinergia
                    sitio_link = '<a href="'+settings.URL+'">www.fundacionmerced.org</a>'
                    evaluacion_link = '<a href="'+settings.URL+'/evaluacion/' + request.POST['sin'] + '">Evaluar sinergia</a>'
                    oscs_link = '<a href="'+settings.URL+'/oscs/">Ver organizaciones</a>'

                    mensaje = request.POST['mensaje']
                    mensaje = mensaje.replace('\n', '<br>')
                    mensaje = mensaje.replace('<sitio_link>', sitio_link)
                    mensaje = mensaje.replace('<osc_nombre>', osc_nombre)
                    mensaje = mensaje.replace('<evaluacion_link>', evaluacion_link)
                    mensaje = mensaje.replace('<oscs_link>', oscs_link)

                    p_end = "</p>"
                    mensaje = p_begin + mensaje + p_end
                    fr = "contacto@fundacionmercedqro.com"
                    t = sinergia.empresa.email
                    mensaje = EmailMessage(subject, mensaje, fr, [t, ])
                    mensaje.content_subtype = "html"  # Main content is now text/html
                    mensaje.send()
                else:
                  no = Sinergia.objects.get(id=request.POST['sin']).id
                  numero = 1
                  sinergias_osc = Sinergia.objects.filter(id=request.POST['sin'])
                  paginator = Paginator(sinergias_osc,1)
                  try:
                    sinergias_osc=paginator.page(page)
                  except PageNotAnInteger:
                    sinergias_osc=paginator.page(1)
                  except EmptyPage:
                    sinergias_osc = paginator.page(paginator.num_pages)

                  template="sinergia_mejorada.html"
                  instance= RequestContext(request,locals())
                  return render_to_response(template,context_instance=instance)

                     

            else: #  Fallo Sinergia 
                forma = SinergiaCancelar(request.POST, request.FILES)
                form_aceptosinergia = AceptoSinergia_Form2()

                if forma.is_valid():
                    Sinergia.objects.filter(id=request.POST['sin_rechazo']).update(comentario=request.POST['comentario'], status='F')
                    sinergia_aceptada = 1

                else:
                    if not ('id_sinergia_editar' in request.POST):
                        var = Sinergia.objects.get(id=request.POST['sin_rechazo']).id
                        sinergias_osc=Sinergia.objects.filter(id=request.POST['sin_rechazo'])
                        paginator= Paginator(sinergias_osc,1)
                        try:
                            sinergias_osc=paginator.page(page)
                        except PageNotAnInteger:
                            sinergias_osc = paginator.page(1)
                        except EmptyPage:
                            sinergias_osc= paginator.page(paginator.num_pages)
                        hola = 1
                        template="sinergia_mejorada.html"
                        instance = RequestContext(request,locals())
                        return render_to_response(template,context_instance=instance)


  ###############################################################################################################################################################################################              



    else: # Si es Get

        forma = SinergiaCancelar()
        form_aceptosinergia = AceptoSinergia_Form2()
      

        if 'busqueda2' in request.GET:

            if request.GET['busqueda2']=="":

                busqueda=""
                estatus2= request.GET['estatus2'];
                
                if request.GET['estatus2']=="T":
                    sinergias_osc= Sinergia.objects.all().filter(osc=osc_id)
                else:
                    sinergias_osc = Sinergia.objects.all().filter(osc=osc_id,status=request.GET['estatus2'])

                sinergias_osc = sinergias_osc.order_by('-pk') 
                page= request.GET.get('page')

            else:

                busqueda= request.GET['busqueda2']
                estatus2= request.GET['estatus2'];
                empresas = Empresa.objects.filter(nombre__icontains=request.GET['busqueda2'])

                if request.GET['estatus2']=="T":
                    sinergias_osc= Sinergia.objects.filter(osc=osc_id,empresa=empresas)
                else:
                    sinergias_osc= Sinergia.objects.filter(osc=osc_id,status=estatus2,empresa=empresas)

                sinergias_osc= sinergias_osc.order_by('-pk')
                page = request.GET.get('page')

        else:

            if number != '':
                sinergias_osc = Sinergia.objects.filter(id=number)
                page = 1

            else:                
                busqueda=""
                estatus2="N"
                
                sinergias_osc = Sinergia.objects.all().filter(osc=osc_id,status=estatus2)
                sinergias_osc = sinergias_osc.order_by('-pk')
                page = request.GET.get('page')









    ### Checa que tipo de usuario es    sinergias_osc=Sinergia.objects.filter(osc=osc_id).order_by('-pk')
          
    #if request.user.is_superuser: # Si es super usuario saca todos
        #sinergia_all = Sinergia.objects.all()

    #else: # Si es usuario osc saca las suyas
        #if number != '':
            #sinergia_all = [Sinergia.objects.get(id=number)]
        #else:
            #i = Afiliacion.objects.filter(user=request.user)
            #sinergia_all = Sinergia.objects.filter(osc__in=i)


############################################# Codigo Raul
    #sinergia_all= sinergia_all.filter(osc=osc_id).order_by('-pk')

    paginator= Paginator(sinergias_osc,25)
    forma = SinergiaCancelar()
    form_aceptosinergia = AceptoSinergia_Form2()

    
    
    try:
        sinergias_osc = paginator.page(page)
    except PageNotAnInteger:
        sinergias_osc = paginator.page(1)
    except EmptyPage:
        sinergias_osc = paginator.page(paginator.num_pages)

############################################# Codigo Raul

    template = "sinergia_mejorada.html"
    instance = RequestContext(request, locals())
    return render_to_response(template, context_instance=instance)
