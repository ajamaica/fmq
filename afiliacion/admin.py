from django.contrib import admin
from models import *
from simple_history.admin import SimpleHistoryAdmin

admin.site.register(Estado)
admin.site.register(Municipio)
admin.site.register(Afiliacion, SimpleHistoryAdmin)
admin.site.register(ImpactoSocial)
admin.site.register(NecesidadesMateriales)
admin.site.register(NecesidadesHumanas)
admin.site.register(NecesidadesCapacitacion)
admin.site.register(Servicios)
admin.site.register(Categoria)
admin.site.register(Asistencia)