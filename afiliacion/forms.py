from django import forms
from afiliacion.models import *
from django.forms.models import inlineformset_factory,formset_factory,modelformset_factory
from betterforms.multiform import MultiModelForm

class ImpactoSocialForm(forms.ModelForm):
	class Meta:
		model = ImpactoSocial
		fields = ('bebes', 'ninas', 'ninos', 'jovenes', 'adultas', 'adultos', 'mayores_mujeres', 'mayores_hombres', 'colaboradores',
			'asalariados', 'voluntarios', 'fuente_ingresos', 'fuente_ingresos_FMQ', 'costo_anual')

class AfiliacionForm(forms.ModelForm):
	class Meta:
		model = Afiliacion
		fields = ('nombre', 'figura', 'estado', 'municipio', 'ano_constitucion', 'ano_constitucion', 'rfc', 'donatoria_internacional', 'cluni', 'donatoria_nacional', 'distintivo_y_cemfi',
			'actividades', 'tematica','domicilio_legal','telefono', 'email', 'website', 'facebook', 'twitter', 'presidente', 'responsable', 'cargo_responsable',
			'email_responsable','fondo', 'logo', 'mision', 'vision', 'pertenece_red','nombre_red', 'convenio', 'representante_autirizo')

class EstadoForm(forms.ModelForm):
	class Meta:
		model = Estado
		fields = ('nombre',)

class MunicipioForm(forms.ModelForm):
	class Meta:
		model = Municipio
		fields = ('nombre',)

class NecesidadesMaterialesForm(forms.ModelForm):
	descripcion = forms.CharField(widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = NecesidadesMateriales
		exclude=("afiliacion",)

class NecesidadesHumanasForm(forms.ModelForm):
	descripcion = forms.CharField(widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = NecesidadesHumanas
		exclude=("afiliacion",)

class NecesidadesCapacitacionForm(forms.ModelForm):
	descripcion = forms.CharField(widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = NecesidadesCapacitacion
		exclude=("afiliacion",)

class ServiciosForm(forms.ModelForm):
	descripcion = forms.CharField(widget=forms.Textarea(attrs={'rows':4,}))
	class Meta:
		model = Servicios
		fields = ('nombre', 'descripcion')
		exclude = ('afiliacion',)

class CategoriaForm(forms.ModelForm):
	class Meta:
		model = Categoria
		exclude = ('afiliacion',)

class AfiliacionMultiForm(MultiModelForm):
	form_classes = {
		'afiliacion' : AfiliacionForm,
		'impacto_social' : ImpactoSocialForm,
	}
	
	def save(self, commit=True):
            objects = super(AfiliacionMultiForm, self).save(commit=False)
            if commit:
                afiliacion = objects['afiliacion']
                impacto_social = objects['impacto_social']                
                impacto_social.save()
                afiliacion.impacto_social = impacto_social
                afiliacion.save()
            return objects

class AsistenciaForm(forms.ModelForm):
	class Meta:
		model=Asistencia
		exclude=('afiliacion',)
            
NecesidadesMaterialesFormset = modelformset_factory(NecesidadesMateriales,extra=1, exclude = ('afiliacion',))
NecesidadesHumanasFormset = modelformset_factory(NecesidadesHumanas,extra=1,exclude = ('afiliacion',))
NecesidadesCapacitacionset = modelformset_factory(NecesidadesCapacitacion,extra=1,exclude = ('afiliacion',))
ServiciosFormset = modelformset_factory(Servicios,extra=1,exclude = ('afiliacion',))
